import React, {useEffect} from 'react';
import Item from './Item';

const List = ({users}) => {
  useEffect (() => {
		console.log("List Render");
	})
	
  return (
    <>
      <h3 className="text-center">List</h3>
      <ul className="list-group">
      {
        users.map(user=>(
          <Item key={user.id} user={user} />
        ))
      }
		</ul>
    </>

  );
}

export default List;