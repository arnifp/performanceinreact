import React, { useState, useEffect } from 'react';
import List from './List';

const initialUsers = [
  {id: 1, name: 'Pepito'},
  {id: 2, name: 'Carlitos'},
  {id: 3, name: 'Chavito'}
];

function App() {

  const [users, setUsers] = useState(initialUsers);
  const [text, setText] = useState('');

  const handledAdd = () => {
    const newUser = {id: Date.now(), name:text}
    setUsers([...users, newUser]);
  }

  useEffect(() => {
    console.log("App Render");
  });

  return (
    <div className="container mt-3">
      <div className="row">
        <div className="col">
          <List users={users} />
        </div>
        <div className="col">
          <input
            className="form-control mb-3"
            type="text"
            placeholder="New name"
            value={text}
            onChange={(e)=>setText(e.target.value)}
          />
          <button
            className="btn btn-success"
            onClick={handledAdd}
          >
            Add
          </button>

          {/* <button
            className="btn btn-primary"
          >
            Search
          </button> */}
        </div>
      </div>
    </div>
  );
}

export default App;
