import React, {useEffect}from 'react';

const Item = ({user}) => {

  useEffect (() => {
		console.log("Item Render");
  })
  
  return (
    <li className="list-group-item li-list">
      <span>
        {user.name}
      </span>
      {/* <button
        className="btn btn-danger"
      >
        Delete
      </button> */}
    </li>
  );
}

export default Item;