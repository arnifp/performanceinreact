# memo, useMemo(), useCallback()

`git clone git@gitlab.com:arnifp/performanceinreact.git`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Tool required

Install in your browser [React developer tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)

## branch basic-example

In this **branch** the example does not implement any optimization.

## branch memo

In this **branch** implements the **REACT* memo function

>For the case of a pure class components it would be as follows.

```js
class Welcome extends React.PureComponent {  
  render() {
    return <h1>Welcome</h1>
  }
}
```

## branch use-memo

This **branch** implements `useMemo()` in a `handleSearch` search function.

## branch use-callback

This **branch** implements `useCallback()` in a `handleDelete` search function.

## Images Tool

![image 1](./assets/tool-images/1.jpeg)

![image 2](./assets/tool-images/2.jpeg)

![image 3](./assets/tool-images/3.jpeg)

![image 4](./assets/tool-images/4.jpeg)

![image 5](./assets/tool-images/5.jpeg)

![image 6](./assets/tool-images/6.jpeg)
